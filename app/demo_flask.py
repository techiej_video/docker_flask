from flask import Flask, jsonify, request

from flask_restful import Resource, Api
import math

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return {"request type": "GET: helloooo world!"}, 200 # no need to use jsonify() here. it appears the format was automatically converted.

    def post(self):
        some_json = request.get_json() # parse the input data as json format
        return {"api received": some_json },200

class Multi(Resource):
    def get(self, num):
        return {'result': num*100}, 200


api.add_resource(HelloWorld, '/')
api.add_resource(Multi,'/multi/<int:num>')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
