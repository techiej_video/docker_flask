FROM python:3.7

RUN mkdir -p /home/app

COPY /app/. /home/app

RUN pip install -r /home/app/requirements.txt

CMD ["python", "/home/app/demo_flask.py" ]

# WORKDIR /app

# COPY . .

# RUN pip install -r requirements.txt

# CMD ["python", "demo_flask.py" ]